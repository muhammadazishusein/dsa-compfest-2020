import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import urllib.request

def read_data(source):
    '''
    Function to input data to Pandas.DataFrame object. It could get data from csv file or from http url. It will return Pandas.DataFrame object

    PARAM:
    - source => fill file path for csv file or url for http

        ex: read_data('file.csv')
            read_data('https://data-corona.co.id')
    '''

    if source.endswith('csv'):
        file = pd.read_csv(source, sep = ',')
        return file
    elif source.startswith('http') or source.startswith('https'):
        file = pd.read_html(source)[0]
        pd.set_option('display.max_rows', None)
        return file
    else:
        return "file format didn't correct"
        return None
    
def filter_data_integer(source, opcode, col, val):
    '''
    Function to filter content in the DataFrame object. It will return filtered DataFrame.

    PARAM:
    - source => source DataFrame
    - opcode => comparison such as lt, le, etc
    - col => specific column to be compared with value
    - val => a value to be compared with each content in the specific column

        ex: filter_data(data, gt, 'total_unit', 5)
    '''

    if opcode == 'lt':
        condition = source[col] < val
        return source[condition]
    if opcode == 'le':
        condition = source[col] <= val
        return source[condition]
    if opcode == 'gt':
        condition = source[col] > val
        return source[condition]
    if opcode == 'ge':
        condition = source[col] >= val
        return source[condition]
    if opcode == 'eq':
        condition = source[col] == val
        return source[condition]
    if opcode == 'ne':
        condition = source[col] != val
        return source[condition]
    if opcode == None:
        return None
    print("this command isn't available Dude")
    return None

def filter_data_string(source, opcode, col, val):
    '''
    Function to filter content in the DataFrame object. It will return filtered DataFrame.

    PARAM:
    - source => source DataFrame
    - opcode => comparison such as lt, le, etc
    - col => specific column to be compared with value
    - val => a value to be compared with each content in the specific column

        ex: filter_data(data, eq, 'name', 'nama')
    '''

    if opcode == 'sw':
        condition = source[col].str.startswith(val)
        return source[condition]
    if opcode == 'ew':
        condition = source[col].str.endswith(val)
        return source[condition]
    if opcode == 'eq':
        condition = source[col].str.startswith(val) == True
        return source[condition]
    if opcode == 'ne':
        condition = source[col].str.endswith(val) == False
        return source[condition]
    if opcode == None:
        return None

    print("this command isn't available Haha")
    return None

def bar_chart(sourcex, sourcey, title, labx, laby):
    if sourcex is not None and sourcey is not None:
        plt.figure(figsize = (12, 5))
        plt.bar(sourcex, sourcey, color = 'blue', edgecolor = 'black')
        plt.xlabel(labx)
        plt.xticks(rotation = 90)
        plt.ylabel(laby)
        plt.title(title)
        image = plt.gcf()
        plt.show()
        image.savefig(title + '.png')
    else:
        return "Invalid Data"

def histogram(sourcedata, totalhist, title, labx, laby):
    if sourcedata is not None:
        plt.figure(figsize = (12, 5))
        plt.hist(sourcedata, bins = totalhist, edgecolor = 'black')
        plt.xlabel(labx)
        plt.ylabel(laby)
        plt.title(title)
        image = plt.gcf()
        plt.show()
        image.savefig(title + '.png')
    else:
        return 'Invalid Data'

def piechart(data, labdat, title):
    if data is not None or labdat is not None :
        plt.figure(figsize = (24, 10))
        patches, texts = plt.pie(data)
        plt.legend(patches, labdat)
        plt.title(title)
        image = plt.gcf()
        plt.show()
        image.savefig(title + '.png')
    else:
        return 'Invalid Data'

def piechart2(values, labels, title):
    plt.figure(figsize = (24, 10))
    plt.pie(values, labels = labels, autopct = '%1.1f%%')
    plt.title(title)
    image = plt.gcf()
    plt.show()
    image.savefig(title + '.png')

def boxplot(sourcedata, title, labx, laby) :
    if sourcedata is not None:
        fig = plt.figure(1, figsize = (10, 40))
        ax = fig.add_subplot(111)
        bp = ax.boxplot(sourcedata)
        plt.yticks(np.arange(min(sourcedata), max(sourcedata)+1, 100000.0))
        ax.set_xlabel(labx)
        ax.set_ylabel(laby)
        ax.set_title(title)
        image = plt.gcf()
        plt.show()
        image.savefig(title + '.png')
    else:
        return 'Invalid Data'

def scatterplot(title, x, y, labx, laby) :
    plt.figure(figsize = (20, 40))
    plt.scatter(x, y)
    plt.yticks(np.arange(min(y), max(y)+1, 100000.0))
    plt.xlabel(labx)
    plt.ylabel(laby)
    plt.title(title)
    image = plt.gcf()
    plt.show()
    image.savefig(title + '.png')