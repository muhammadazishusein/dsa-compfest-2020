# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# # DATA SCIENCE ACADEMY (COMPFEST 12) - ELIMINATION
# 
# ## Rubemsky:
# - Muhammad Azis Husein
# - Muhammad Mudrik
# - Hanna Jannatuna'im
# %% [markdown]
# # KETERANGAN SOAL
# 
# Sumber dataset: https://www.kaggle.com/avikasliwal/used-cars-price-prediction?select=train-data.csv (dengan sedikit modifikasi)
# 
# - Name - Nama merek dan model mobil
# - Location - Lokasi mobil dijual
# - Year - Tahun edisi mobil
# - Kilometers_Driven - Total jarak pemakaian mobil oleh pengguna sebelumnya dalam satuan kilometer
# - Fuel_Type - Tipe bahan bakar yang digunakan mobil (Petrol/Diesel/Electric/CNG/LPG)
# - Transmission - Tipe transmisi yang digunakan mobil (Automatic/Manual)
# - Owner_Type - Jenis kepemilikan mobil (First/Second/Third/Fourth & Above)
# - Mileage - Tingkat konsumsi bahan bakar dalam satuan kmpl atau km/kg
# - Engine - Kapasitas mesin mobil dalam satuan CC
# - Power - Tenaga maksimum dari mesin mobil dalam satuan bhp
# - Seats - Jumlah kursi pada mobil
# - Price - Harga mobil bekas dalam satuan INR Lakhs (Indian Rupee)
# %% [markdown]
# # SOAL
# 
# 1. Merek mobil apa saja yang tersedia dan ada berapa banyak mobil untuk tiap merek tersebut?
# 2. Kota apa yang memiliki mobil bekas paling banyak?
# 3. Bagaimana distribusi tahun edisi mobil-mobil bekas tersebut?
# 4. Berapa banyak mobil yang memiliki total jarak pemakaian di bawah 100.000 kilometer?
# 5. Pada batas berapa kilometer total jarak pemakaian bisa dikategorikan sebagai rendah atau tinggi? Sertakan argumen yang mendukung jawaban.
# 6. Apakah terdapat outlier pada kolom Kilometers_Driven? Sertakan argumen yang mendukung jawaban.
# 7. Apakah tahun pembuatan mobil berpengaruh terhadap total jarak pemakaian? Sertakan argumen yang mendukung jawaban.
# 8. Berapa banyak mobil yang merupakan kepemilikan ketiga atau lebih?
# 9. Tipe bahan bakar apa yang memiliki mileage (konsumsi bahan bakar) paling hemat?
# 10. Apa saja faktor-faktor yang mempengaruhi harga mobil bekas di India? Sertakan argumen yang mendukung jawaban.
# %% [markdown]
# # IMPORT MODULE

# %%
import processor as pc

# %% [markdown]
# # READ DATA

# %%
data = baca_data('used_car_data.csv')
data

# %% [markdown]
# # SOLUSI
# %% [markdown]
# ### Merek mobil apa saja yang tersedia dan ada berapa banyak mobil untuk tiap merek tersebut

# %%
dict_merk = {}

list_merk = pc.potong_data(data, 'Name', 'Name')

for merk in list_merk:
    merk = merk.split()[0]
    if merk not in dict_merk.keys():
        dict_merk[merk] = 1
    else:
        dict_merk += 1

dict_merk

